/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package ru.t1.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.t1.volkova.tm.client.TaskRestEndpointClient;
import ru.t1.volkova.tm.dto.TaskDTO;
import ru.t1.volkova.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author aavolkova
 */
public class TaskTests {

    private static final long SIZE = 5;
    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/task";
    @NotNull
    private static final RestTemplate template = new RestTemplate();
    @NotNull
    private static final HttpHeaders headers = new HttpHeaders();
    private static TaskRestEndpointClient client;
    @Nullable
    private static List<TaskDTO> taskList;

    @BeforeClass
    public static void initTest() {
        headers.setContentType(MediaType.APPLICATION_JSON);
        client = new TaskRestEndpointClient(BASE_URL, template, headers);
        createTasks();
    }

    public static void createTasks() {
        taskList = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            @NotNull TaskDTO task = new TaskDTO();
            task.setName("Task" + i);
            task.setDescription("description" + i);
            client.save(task);
            taskList.add(task);
        }
    }

    @AfterClass
    public static void deleteTasks() {
        if (taskList == null) return;
        for (final TaskDTO task : taskList) {
            client.deleteById(task.getId());
        }
    }

    // PROJECT TESTS

    //GET
    @Test
    @Category(IntegrationCategory.class)
    public void taskFindByIdTest() {
        final TaskDTO task = client.findById(taskList.get(0).getId());
        assertEquals(taskList.get(0).getId(), task.getId());
    }

    //POST
    @Test
    @Category(IntegrationCategory.class)
    public void taskCreateTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("TaskCreated");
        task.setDescription("descriptionCreated");
        client.save(task);
        taskList.add(task);
        assertEquals(client.findAll().size(), taskList.size());
    }

    //UPDATE
    @Test
    @Category(IntegrationCategory.class)
    public void taskUpdateTest() {
        final TaskDTO task = taskList.get(0);
        task.setName("TaskNew");
        task.setDescription("descriptionNew");
        client.update(task);
        TaskDTO taskUpdated = client.findById(task.getId());
        assertEquals(taskList.size(), SIZE);
        assertEquals(task.getName(), taskUpdated.getName());
        assertEquals(task.getDescription(), taskUpdated.getDescription());
    }

    //DELETE
    @Test
    @Category(IntegrationCategory.class)
    public void taskDeleteByidTest() {
        client.deleteById(taskList.get(1).getId());
        final TaskDTO task = client.findById(taskList.get(1).getId());
        assertNull(task);
    }

    // PROJECT COLLECTION TESTS

    //GET
    @Test
    @Category(IntegrationCategory.class)
    public void taskFindAllTest() {
        final List<TaskDTO> tasks = client.findAll();
        assertNotEquals(0, tasks.size());
    }

    //POST
    @Test
    @Category(IntegrationCategory.class)
    public void taskCreateListTest() {
        final long newSize = 2;
        final List<TaskDTO> newTaskList = new ArrayList<TaskDTO>();
        for (int i = 0; i < newSize; i++) {
            @NotNull TaskDTO task = new TaskDTO();
            task.setName("Task_test" + i);
            task.setDescription("description_test" + i);
            newTaskList.add(task);
            taskList.add(task);
        }
        client.saveAll(newTaskList);
        final long size = client.findAll().size();
        assertEquals(size, SIZE + newSize);
    }

    //UPDATE
    @Test
    @Category(IntegrationCategory.class)
    public void taskUpdateAllTest() {
        final String NewName = "UpdName";
        final String NewDesc = "UpdDesc";
        for (final TaskDTO task : taskList) {
            task.setName(NewName);
            task.setDescription(NewDesc);
        }
        client.saveAll(taskList);
        final List<TaskDTO> updTaskList = client.findAll();
        for (final TaskDTO task : updTaskList) {
            assertEquals(task.getName(), NewName);
            assertEquals(task.getDescription(), NewDesc);
        }
    }

    //DELETE
    @Test
    @Category(IntegrationCategory.class)
    public void taskDeleteAllTest() {
        client.deleteAll();
        assertEquals(0, client.findAll().size());
        createTasks();
    }

}
