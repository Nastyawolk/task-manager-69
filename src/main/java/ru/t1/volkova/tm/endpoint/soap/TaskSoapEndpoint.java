package ru.t1.volkova.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.dto.*;

import java.util.List;

@Endpoint
public class TaskSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.volkova.t1.ru/dto";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse taskFindById(@RequestPayload final TaskFindByIdRequest request) {
        String id = request.getId();
        return new TaskFindByIdResponse(taskService.findById(id));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    public TaskCreateResponse taskCreate(@RequestPayload final TaskCreateRequest request) {
        TaskDTO task = request.getTask();
        taskService.create(task);
        return new TaskCreateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    public TaskUpdateResponse taskUpdate(@RequestPayload final TaskUpdateRequest request) {
        TaskDTO task = request.getTask();
        taskService.update(task);
        return new TaskUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse taskDeleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        String id = request.getId();
        taskService.deleteById(id);
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse taskFindAll(@RequestPayload final TaskFindAllRequest request) {
        TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTask(taskService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateAllRequest", namespace = NAMESPACE)
    public TaskCreateAllResponse taskCreateAll(@RequestPayload final TaskCreateAllRequest request) {
        @NotNull List<TaskDTO> tasks = request.getTask();
        taskService.saveAll(tasks);
        return new TaskCreateAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateAllRequest", namespace = NAMESPACE)
    public TaskUpdateAllResponse taskUpdateAll(@RequestPayload final TaskUpdateAllRequest request) {
        @NotNull List<TaskDTO> tasks = request.getTask();
        taskService.saveAll(tasks);
        return new TaskUpdateAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse taskDeleteAll(@RequestPayload final TaskDeleteAllRequest request) {
        taskService.removeAll();
        return new TaskDeleteAllResponse();
    }

}
