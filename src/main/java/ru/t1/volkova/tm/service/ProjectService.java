package ru.t1.volkova.tm.service;

import ru.t1.volkova.tm.api.service.IProjectService;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.IProjectDTORepository;
import ru.t1.volkova.tm.dto.ProjectDTO;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectDTORepository projectRepository;

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    @Override
    public void saveAll(@NotNull List<ProjectDTO> projects) {
        projectRepository.saveAll(projects);
    }

    @Transactional
    @Override
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable String id) {
        if (id == null) return null;
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    @Transactional
    @Override
    public ProjectDTO create(@Nullable ProjectDTO project) {
        if (project == null) {
            return null;
        }
        return projectRepository.save(project);
    }

    @Nullable
    @Transactional
    @Override
    public ProjectDTO updateById(@Nullable String id) {
        if (id == null) return null;
        @Nullable final ProjectDTO project = findById(id);
        if (project == null) {
            return null;
        }
        return projectRepository.save(project);
    }

    @Nullable
    @Transactional
    @Override
    public ProjectDTO update(@Nullable ProjectDTO project) {
        if (project == null) {
            return null;
        }
        return projectRepository.save(project);
    }

    @Transactional
    @Override
    public void deleteById(@Nullable String id) {
        if (id == null) return;
        projectRepository.deleteById(id);
    }

}
