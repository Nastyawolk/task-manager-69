package ru.t1.volkova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.IDatabaseProperty;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IDatabaseProperty {

    @NotNull
    @Value("#{environment['database.username']}")
    public String databaseUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    public String databasePassword;

    @NotNull
    @Value("#{environment['database.url']}")
    public String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLvlCache;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseRegionPrefix;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseProviderCfgFileResPath;

    @NotNull
    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseRegionFactoryClass;

}
