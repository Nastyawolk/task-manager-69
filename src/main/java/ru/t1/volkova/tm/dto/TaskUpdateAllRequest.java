package ru.t1.volkova.tm.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "task"
})
@XmlRootElement(name = "taskUpdateAllRequest")
public class TaskUpdateAllRequest {

    protected List<TaskDTO> task;

    public List<TaskDTO> getTask() {
        if (task == null) {
            task = new ArrayList<TaskDTO>();
        }
        return this.task;
    }

}
