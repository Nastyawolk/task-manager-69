package ru.t1.volkova.tm.dto;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "project"
})
@XmlRootElement(name = "projectCreateAllRequest")
@Setter
public class ProjectCreateAllRequest {

    @XmlElement(required = true)
    protected List<ProjectDTO> project;


    public List<ProjectDTO> getProject() {
        if (project == null) {
            project = new ArrayList<>();
        }
        return this.project;
    }

}
