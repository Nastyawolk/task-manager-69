package ru.t1.volkova.tm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id"
})
@XmlRootElement(name = "taskDeleteByIdRequest")
@Getter
@Setter
public class TaskDeleteByIdRequest {

    @XmlElement(required = true)
    protected String id;

}
