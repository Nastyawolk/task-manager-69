package ru.t1.volkova.tm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "task"
})
@XmlRootElement(name = "taskCreateRequest")
@Setter
@Getter
public class TaskCreateRequest {

    @XmlElement(required = true)
    protected TaskDTO task;

}
