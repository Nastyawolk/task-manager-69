package ru.t1.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "project"
})
@XmlRootElement(name = "projectFindByIdResponse")
@Setter
@Getter
@NoArgsConstructor
public class ProjectFindByIdResponse {

    protected ProjectDTO project;

    public ProjectFindByIdResponse(ProjectDTO project) {
        this.project = project;
    }

}
