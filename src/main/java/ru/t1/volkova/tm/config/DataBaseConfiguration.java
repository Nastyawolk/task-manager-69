package ru.t1.volkova.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import ru.t1.volkova.tm.api.IDatabaseProperty;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.volkova.tm")
@EnableJpaRepositories("ru.t1.volkova.tm.api.repository")
public class DataBaseConfiguration {

    @NotNull
    @Autowired
    private IDatabaseProperty databaseProperty;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseUrl());
        dataSource.setUsername(databaseProperty.getDatabaseUsername());
        dataSource.setPassword(databaseProperty.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.volkova.tm.dto");
        @NotNull final Properties properties = new Properties();
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecondLvlCache());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseRegionFactoryClass());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseProviderCfgFileResPath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
