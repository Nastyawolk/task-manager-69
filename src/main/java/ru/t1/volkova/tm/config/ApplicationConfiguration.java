package ru.t1.volkova.tm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.volkova.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.t1.volkova.tm.endpoint.soap.TaskSoapEndpoint;

@ComponentScan("ru.t1.volkova.tm")
@EnableWs
@Configuration
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdl11Definition(XsdSchema projectEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdl11Definition(XsdSchema taskEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

}
