package ru.t1.volkova.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.t1.volkova.tm.dto.TaskDTO;

import java.util.List;

public class TaskRestEndpointClient {

    @NotNull
    private final String BASE_URL;

    @NotNull
    private final RestTemplate template;

    @NotNull
    private final HttpHeaders headers;

    public TaskRestEndpointClient(
            @NotNull final String BASE_URL,
            @NotNull final RestTemplate template,
            @NotNull final HttpHeaders headers
    ) {
        this.BASE_URL = BASE_URL;
        this.template = template;
        this.headers = headers;
    }

    // Task METHODS

    @Nullable
    public TaskDTO findById(@NotNull final String id) {
        return template.getForObject(BASE_URL + "/{id}", TaskDTO.class, id);
    }

    @Nullable
    public TaskDTO save(@NotNull final TaskDTO task) {
        final HttpEntity entity = new HttpEntity(task, headers);
        return template.postForObject(BASE_URL, entity, TaskDTO.class);
    }

    public void update(@NotNull final TaskDTO task) {
        final HttpEntity entity = new HttpEntity(task, headers);
        template.put(BASE_URL, entity, TaskDTO.class);
    }

    public void deleteById(@NotNull final String id) {
        template.delete(BASE_URL + "/{id}", id);
    }

    // PROJECT COLLECTION METHODS

    @Nullable
    public List<TaskDTO> findAll() {
        ResponseEntity<List<TaskDTO>> taskResponse =
                template.exchange(BASE_URL + "s",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<TaskDTO>>() {
                        });
        return taskResponse.getBody();
    }

    public void saveAll(@NotNull final List<TaskDTO> tasks) {
        final HttpEntity entity = new HttpEntity(tasks, headers);
        ResponseEntity<List<TaskDTO>> taskResponse =
                template.exchange(BASE_URL + "s",
                        HttpMethod.POST, entity, new ParameterizedTypeReference<List<TaskDTO>>() {
                        });
    }

    public void updateAll(@NotNull final List<TaskDTO> tasks) {
        final HttpEntity entity = new HttpEntity(tasks, headers);
        ResponseEntity<List<TaskDTO>> taskResponse =
                template.exchange(BASE_URL + "s",
                        HttpMethod.PUT, entity, new ParameterizedTypeReference<List<TaskDTO>>() {
                        });
    }

    public void deleteAll() {
        template.delete(BASE_URL + "s");
    }

}
