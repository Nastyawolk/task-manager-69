package ru.t1.volkova.tm.api.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.dto.TaskDTO;

public interface ITaskService {

    @Nullable
    @Transactional
    TaskDTO create(@Nullable TaskDTO task);

    @Transactional
    void deleteById(@Nullable String id);

    @NotNull
    List<TaskDTO> findAll();

    @Nullable
    TaskDTO findById(@Nullable String id);

    @Transactional
    void removeAll();

    @Transactional
    void saveAll(@NotNull List<TaskDTO> projects);

    @Nullable
    @Transactional
    TaskDTO updateById(@Nullable String id);

    @Nullable
    @Transactional
    TaskDTO update(@Nullable TaskDTO task);

}
