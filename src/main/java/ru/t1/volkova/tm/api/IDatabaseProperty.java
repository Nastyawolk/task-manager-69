/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ru.t1.volkova.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {
    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUrl();

    @NotNull String getDatabaseUsername();

    @NotNull String getDatabaseDriver();

    @NotNull String getDatabaseDialect();

    @NotNull String getDatabaseHbm2ddlAuto();

    @NotNull String getDatabaseShowSql();

    @NotNull String getDatabaseUseSecondLvlCache();

    @NotNull String getDatabaseUseQueryCache();

    @NotNull String getDatabaseUseMinimalPuts();

    @NotNull String getDatabaseRegionPrefix();

    @NotNull String getDatabaseProviderCfgFileResPath();

    @NotNull String getDatabaseRegionFactoryClass();

}
