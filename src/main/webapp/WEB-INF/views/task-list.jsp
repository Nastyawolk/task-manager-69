<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="../include/_header.jsp" />
<h1>TASK LIST</h1>
<table width="100%" border="1" style="border-collapse: collapse">
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>DESCRIPTION</th>
        <th>PROJECT ID</th>
        <th>STATUS</th>
        <th>START</th>
        <th>FINISH</th>
        <th>EDIT</th>
        <th>DELETE</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr align="center">
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.projectId}"/>
            </td>
            <td>
                <c:out value="${task.status}"/>
            </td>
            <td>
                <f:formatDate pattern="dd.MM.yyyy" value="${task.dateStart}" />
            </td>
            <td>
                <f:formatDate pattern="dd.MM.yyyy" value="${task.dateFinish}" />
            </td>
            <td>
                <a href="/task/edit/${task.id}">EDIT</a>
            </td>
            <td>
                <a href="/task/delete/${task.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/task/create">
    <button>CREATE TASK</button>
</form>
<jsp:include page="../include/_footer.jsp" />